
#include "Arduino.h"

#define LED_BUILTIN 2

#ifndef UNIT_TEST  // IMPORTANT LINE!

/**
 * \brief Setup code
 * 
 * This block of code runs once, when the Arduino is powered up.
 */
void setup() {
  // initialize LED digital pin as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

/**
 * ... text ...
 */
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);
  delay(1000);
  digitalWrite(LED_BUILTIN, LOW);
  delay(1000);
}

#endif    // IMPORTANT LINE!
