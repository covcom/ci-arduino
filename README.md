# CI Toolchain Notes

[![Build Status](https://gitlab.com/covcom/ci-arduino/badges/master/build.svg)](https://gitlab.com/covcom/ci-arduino/commits/master)
[![Coverage Report](https://gitlab.com/covcom/ci-arduino/badges/master/coverage.svg)](https://gitlab.com/pantomath-io/demo-tools/commits/master)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/covcom/ci-arduino)](https://goreportcard.com/report/gitlab.com/pantomath-io/demo-tools)
[![License MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://img.shields.io/badge/License-MIT-brightgreen.svg)

Here are some useful build steps to install packages using the package manager and installing using a downloaded .deb installer.

```yaml
- apt-get install -y wget build-essential
- wget http://ftp.br.debian.org/debian/pool/main/c/cppcheck/cppcheck_1.76.1-1_i386.deb
- dpkg-reconfigure debconf -f noninteractive -p critical
- dpkg -i cppcheck_1.76.1-1_i386.deb
```

`build-essential` installs the `gcc` compiler and other related tools.